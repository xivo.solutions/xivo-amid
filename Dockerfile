FROM python:3.11-slim-bookworm
MAINTAINER XiVO Team "dev@proformatique.com"

# Add dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends --auto-remove \
    git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ADD . /usr/src/xivo-amid
ADD ./contribs/docker/certs /usr/share/xivo-certs
WORKDIR /usr/src/xivo-amid
RUN pip install --upgrade pip
RUN pip install -r requirements.txt \
    && python setup.py install \
    && adduser --system --group --quiet --no-create-home --disabled-login xivo-amid \
    && install -o xivo-amid -g xivo-amid /dev/null /var/log/xivo-amid.log \
    && install -d -o xivo-amid -g xivo-amid /var/run/xivo-amid \
    && cp -r etc/* /etc

EXPOSE 9491
CMD ["xivo-amid", "-fd"]
