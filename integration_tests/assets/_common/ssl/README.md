How to generate certificates
============================

Create self-signed certificate for 1 year:

openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -config openssl.cfg -keyout server.key -out server.crt
